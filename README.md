# Summative 4

## Product Collections 

**GET All Data [GET]**  

Routing

```
/api/products
```

**Http Response 200**

Response 

```
{
    "status": 200,
    "message": "Data of Produts Found",
    "data": [
        {
            "id": 33,
            "product_name": "Pocari Sweat",
            "category": "Drinks",
            "stock": 4,
            "price": 15000,
            "created_at": "2021-10-20 11:44:25"
        },
        {
            "id": 34,
            "product_name": "Aqua",
            "category": "Drinks",
            "stock": 148,
            "price": 16000,
            "created_at": "2021-10-20 11:50:46"
        },
        {
            "id": 35,
            "product_name": "Le Mineral",
            "category": "Drinks",
            "stock": 153,
            "price": 6000,
            "created_at": "2021-10-16 11:50:46"
        },
        {
            "id": 36,
            "product_name": "Yupi Burger",
            "category": "Candys",
            "stock": 150,
            "price": 3000,
            "created_at": "2021-08-10 11:50:46"
        },
        {
            "id": 37,
            "product_name": "Apenliebe Original",
            "category": "Candys",
            "stock": 160,
            "price": 6000,
            "created_at": "2021-10-20 11:50:46"
        },
        {
            "id": 38,
            "product_name": "Kopiko",
            "category": "Candys",
            "stock": 18,
            "price": 7000,
            "created_at": "2021-10-20 11:50:46"
        }
    ]
}
```

**Http Response 500**

Response 

```
{
    "status": 500,
    "message": "Data Not Found",
    "data": []
}
```



**POST NewData [POST]** 

Routing 

```
/api/product
```

**Http Response 201**

Body

```
[{
    "product_name" : "Roma Sari Gandum",
    "category" : "Snacks",
    "stock" : 90,
    "price" : 50000
},
{   "product_name" : "Pillows Chocolate",
    "category" : "Snacks",
    "stock" : 90,
    "price" : 10000
}
]
```

Response

```
{
    "status": 201,
    "message": "DATA CREATED",
    "data": [
        {
            "id": 39,
            "product_name": "Roma Sari Gandum",
            "category": "Snacks",
            "stock": 90,
            "price": 50000,
            "created_at": "2021-10-29 19:14:29"
        },
        {
            "id": 40,
            "product_name": "Pillows Chocolate",
            "category": "Snacks",
            "stock": 90,
            "price": 10000,
            "created_at": "2021-10-29 19:14:29"
        }
    ]
}
```

**Http Response 500**

Body

```
[{
    "product_name" : "Roma Sari Gandum",
    "category" : "Snacks",
    "stock" : 0,
    "price" : 50000
},
{   "product_name" : "Pillows Chocolate",
    "category" : "Snacks",
    "stock" : 90,
    "price" : 10000
}
]
```

Response

```
{
    "status": 500,
    "message": "CREATE DATA FAILED : Stock Invalid, Input must be at least 1 or All Fields must be inputed",
    "data": []
}
```



**EDIT Data [PUT]**

Routing

```
/api/product/{pathvariable}
```

**Http Response 200**

Path Variable & Body 

```
/api/product/33
```

```
{
    "product_name" : "Richeese Wafer",
    "category" : "Snacks",
    "stock" : 70,
    "price" : 80000
}
```

Response

```
{
    "status": 201,
    "message": "EDIT SUCCESS",
    "data": [
        {
            "id": 33,
            "product_name": "Richeese Wafer",
            "category": "Snacks",
            "stock": 70,
            "price": 80000,
            "created_at": "2021-10-20 11:44:25"
        }
    ]
}
```

**Http Response 500**

Path Variable & Body 

```
/api/product/33
```

```
{
    "product_name" : "Richeese Wafer",
    "category" : "Snacks",
    "stock" : 0,
    "price" : 80000
}
```

Response

```
{
    "status": 500,
    "message": "EDIT FAILED, Stock Invalid",
    "data": []
}
```



**DELETE Data [DELETE]** 

Routing

```
/api/product/pathvariable
```

**Http Response 200**

PathVariable

```
http://localhost:8081/api/product/33
```

Response

```
{
    "status": 201,
    "message": "DELETE SUCCESS",
    "data": [
        {
            "id": 33,
            "product_name": "Richeese Wafer",
            "category": "Snacks",
            "stock": 70,
            "price": 80000,
            "created_at": "2021-10-20 11:44:25"
        }
    ]
}
```

**Http Response 500**

PathVariable

```
http://localhost:8081/api/product/339
```

Response

```
{
    "status": 500,
    "message": "DELETE FAILED : ID not found",
    "data": []
}
```



**SEARCH Data  [POST]**

Routing

```
/api/products/search
```

**Http Response 200 By Stock**

Parameter

```
/api/products/search?stock=150
```

Response

```
{
    "status": 201,
    "message": "DATA FOUND",
    "data": [
        {
            "id": 36,
            "product_name": "Yupi Burger",
            "category": "Candys",
            "stock": 150,
            "price": 3000,
            "created_at": "2021-08-10 11:50:46"
        }
    ]
}
```

**Http Response 500**

Path Variable

```
http://localhost:8081/api/products/search?stock=1509
```

 Response

```
{
    "status": 500,
    "message": "DATA NOT FOUND",
    "data": []
}
```

**Http Response 200 By Name**

Parameter

```
/api/products/search?name=Aqua
```

Response

```
{
    "status": 201,
    "message": "DATA FOUND",
    "data": [
        {
            "id": 34,
            "product_name": "Aqua",
            "category": "Drinks",
            "stock": 141,
            "price": 16000,
            "created_at": "2021-10-20 11:50:46"
        }
    ]
}
```

**Http Response 200 By Category**

Parameter

```
/api/products/search?category=Drinks
```

Response

```
{
    "status": 201,
    "message": "DATA FOUND",
    "data": [
        {
            "id": 34,
            "product_name": "Aqua",
            "category": "Drinks",
            "stock": 141,
            "price": 16000,
            "created_at": "2021-10-20 11:50:46"
        },
        {
            "id": 35,
            "product_name": "Le Mineral",
            "category": "Drinks",
            "stock": 150,
            "price": 6000,
            "created_at": "2021-10-16 11:50:46"
        }
    ]
}
```

**Http Response 200 By Stock & Name**

Parameter

```
/api/products/search?name=Le Mineral&category=Drinks
```

Response

```
{
    "status": 201,
    "message": "DATA FOUND",
    "data": [
        {
            "id": 35,
            "product_name": "Le Mineral",
            "category": "Drinks",
            "stock": 150,
            "price": 6000,
            "created_at": "2021-10-16 11:50:46"
        }
    ]
}
```

**Http Response 200 By Stock & Name & Category**

Parameter

```
/api/products/search?stock=150&name=Le Mineral&category=Drinks
```

Response

```
{
    "status": 201,
    "message": "DATA FOUND",
    "data": [
        {
            "id": 35,
            "product_name": "Le Mineral",
            "category": "Drinks",
            "stock": 150,
            "price": 6000,
            "created_at": "2021-10-16 11:50:46"
        }
    ]
}
```

**Http Response 500**

Path Variable

```
http://localhost:8081/api/products/search?stock=1509
```

 Response

```
{
    "status": 500,
    "message": "DATA NOT FOUND",
    "data": []
}
```



## Transactions Collections 

**GET All Transactions**

Routing

```
/api/transactions
```

**Http Response 200**

Response

```
{
    "status": 200,
    "message": "Data of Produts Found",
    "data": [
        {
            "id": 4,
            "products": {
                "id": 34,
                "product_name": "Aqua",
                "category": "Drinks",
                "stock": 141,
                "price": 16000,
                "created_at": "2021-10-20 11:50:46"
            },
            "qty": 1,
            "total_price": 16000,
            "created_at": "2021-10-29 19:52:00"
        },
        {
            "id": 5,
            "products": {
                "id": 35,
                "product_name": "Le Mineral",
                "category": "Drinks",
                "stock": 150,
                "price": 6000,
                "created_at": "2021-10-16 11:50:46"
            },
            "qty": 2,
            "total_price": 20000,
            "created_at": "2021-10-25 19:52:00"
        },
        {
            "id": 6,
            "products": {
                "id": 34,
                "product_name": "Aqua",
                "category": "Drinks",
                "stock": 141,
                "price": 16000,
                "created_at": "2021-10-20 11:50:46"
            },
            "qty": 2,
            "total_price": 32000,
            "created_at": "2021-10-29 20:05:19"
        }
    ]
}
```

**Http Response 500**

 Response

```
{
    "status": 500,
    "message": "Data Not Found",
    "data": []
}
```



**CREATE Transactions**

Routing

```
/api/transaction
```

**Http Response 200**

Body

```
{
    "products" : {"id": 34},
    "qty" : 2
}
```

Response

```
{
    "status": 201,
    "message": "DATA CREATED",
    "data": [
        {
            "id": 6,
            "products": {
                "id": 34,
                "product_name": "Aqua",
                "category": "Drinks",
                "stock": 141,
                "price": 16000,
                "created_at": "2021-10-20 11:50:46"
            },
            "qty": 2,
            "total_price": 32000,
            "created_at": "2021-10-29 20:05:19"
        }
    ]
}
```

**Http Response 500**

Body

```
{
    "products" : {"id": 34},
    "qty" : 1000
}
```

 Response

```
{
    "status": 500,
    "message": "CREATE TRANSACTION FAILED : Stock Unavailable",
    "data": []
}
```



**SEARCH By Date**

Routing

```
/api/payment/search
```

**Http Response 200**

Body

```
{
    "created_at" : "2021-10-29"
}
```

Response

```
{
    "status": 200,
    "message": "DATA FOUND",
    "data": [
        {
            "id": 4,
            "products": {
                "id": 34,
                "product_name": "Aqua",
                "category": "Drinks",
                "stock": 143,
                "price": 16000,
                "created_at": "2021-10-20 11:50:46"
            },
            "qty": 1,
            "total_price": 16000,
            "created_at": "2021-10-29 19:52:00"
        }
    ]
}
```

**Http Response 500**

Body

```
{
    "created_at" : "2021-10-28"
}
```

 Response

```
{
    "status": 500,
    "message": "DATA NOT FOUND",
    "data": []
}
```



