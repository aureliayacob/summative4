package com.summative4.NexMart.entity;

import javax.persistence.*;

@Entity
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String product_name;
	private String category;
	private int stock;
	private int price;
	private String created_at;
	
	
	protected Product() {
	}
	public Product(String product_name, String category, int stock, int price) {
		super();
		this.product_name = product_name;
		this.category = category;
		this.stock = stock;
		this.price = price;
	}
	
	public Product(String product_name, String category, int stock, int price, String created_at) {
		super();
		this.product_name = product_name;
		this.category = category;
		this.stock = stock;
		this.price = price;
		this.created_at = created_at;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", product_name=" + product_name + ", category=" + category + ", stock=" + stock
				+ ", price=" + price + ", created_at=" + created_at + "]";
	}


}
