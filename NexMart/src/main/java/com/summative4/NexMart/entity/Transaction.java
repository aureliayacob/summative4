package com.summative4.NexMart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



@Entity
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToOne
	@JoinColumn(name="product_id") //??
	private Product products;
	private int qty;
	private int total_price;
	private String created_at; //?? formatter
	public Transaction() {
		super();
	}

	public Transaction(Product products, int qty, int total_price) {
		super();
		this.products = products;
		this.qty = qty;
		this.total_price = total_price;
	}
	
	public Transaction(Product products, int qty, int total_price, String created_at) {
		super();
		this.products = products;
		this.qty = qty;
		this.total_price = total_price;
		this.created_at = created_at;
	}
	
	public int getQty() {
		return qty;
	}
	
	public void setQty(int qty) {
		this.qty = qty;
	}
	
	public int getTotal_price() {
		return total_price;
	}
	
	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}
	public int getId() {
		return id;
	}
	
	public Product getProducts() {
		return products;
	}
	
	public String getCreated_at() {
		return created_at;
	}

	public void setId(int id) {
		this.id = id;
	}


	public void setProducts(Product products) {
		this.products = products;
	}


	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", products=" + products + ", qty=" + qty + ", total_price=" + total_price
				+ ", created_at=" + created_at + "]";
	}

}
