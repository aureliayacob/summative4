package com.summative4.NexMart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NexMartApplication {

	public static void main(String[] args) {
		SpringApplication.run(NexMartApplication.class, args);
		
	}

}
