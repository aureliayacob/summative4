package com.summative4.NexMart.services;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.summative4.NexMart.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction,Long>{

	
	
	@Query(value="SELECT * FROM Transaction t WHERE t.created_at LIKE  %:keyword% ", nativeQuery=true)
	List<Transaction> getTransactionByDate(String keyword);
	
}
