package com.summative4.NexMart.services;

import java.util.*;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.summative4.NexMart.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

	@Query(value="SELECT * FROM Product p", nativeQuery=true)
	List<Product> getAllProduct();
	
	@Query(value="SELECT * FROM Product p where p.id=?1", nativeQuery=true)
	Product getProductById(int id);
	
	@Query(value="SELECT * FROM Product p where p.stock=?1", nativeQuery=true)
	List<Product> getProductbyStock(int stock);
	
	//SELECT * FROM product WHERE product_name LIKE '%Minerale%';
	@Query(value="SELECT * FROM Product p WHERE p.product_name LIKE  %:keyword% ", nativeQuery=true)
	List<Product> getProductbyName(String keyword);
	
	@Query(value="SELECT * FROM Product p WHERE p.category LIKE  %:keyword% ", nativeQuery=true)
	List<Product> getProductbyCategory(String keyword);
	
	@Query(value="SELECT * FROM Product p WHERE p.stock=:stocks AND p.product_name LIKE  %:keyword% ", nativeQuery=true)
	List<Product> getProductbyStockName(int stocks, String keyword);
	
	@Query(value="SELECT * FROM Product p WHERE p.stock=:stocks AND p.category LIKE  %:keyword% ", nativeQuery=true)
	List<Product> getProductbyStockCategory(int stocks, String keyword);
	
	@Query(value="SELECT * FROM Product p WHERE p.product_name LIKE  %:name% AND p.category LIKE  %:category% ", nativeQuery=true)
	List<Product> getProductbyNameCategory(String name, String category);
	
	@Query(value="SELECT * FROM Product p WHERE p.stock=:stocks AND  p.product_name LIKE  %:name% AND p.category LIKE  %:category% ", nativeQuery=true)
	List<Product> getProductbyAllKeys(int stocks, String name, String category);
	
	
	
	@Transactional
	@Modifying
	@Query(value="DELETE FROM Product p where p.id=?1", nativeQuery=true)
	int deleteProductById(int id);
	
	@Transactional
	@Modifying
	@Query(value="INSERT INTO Product (product_name, category, stock, price, created_at) values (?1,?2,?3,?4,?5)", nativeQuery=true)
	int createProduct(String name, String category, int stock, int price, String time);
	
	@Transactional
	@Modifying
	@Query(value="UPDATE product p set p.product_name=?1, p.category=?2, p.stock=?3, p.price=?4 where p.id= ?5 ", nativeQuery=true)
	int updateByID(String name, String category, int stock, int price, int id);
	
	
	@Transactional
	@Modifying
	@Query(value="UPDATE product p set p.stock=?1 where p.id= ?2 ", nativeQuery=true)
	int updateStockByID( int stock, int id);
	

	

	
}
