package com.summative4.NexMart.services;

import java.util.List;

public class Response<T> {
	private int status;
	private String message;
	private List<T> data; //make data helper
	
	public Response(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	
	public Response(int status, String message, List<T> data2) {
		super();
		this.status = status;
		this.message = message;
		this.data = data2;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}


}
