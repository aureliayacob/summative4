package com.summative4.NexMart.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.summative4.NexMart.entity.Product;
import com.summative4.NexMart.entity.Transaction;
import com.summative4.NexMart.services.ProductRepository;
import com.summative4.NexMart.services.Response;
import com.summative4.NexMart.services.TransactionRepository;



@RestController
public class TransactionController {

	@Autowired
	TransactionRepository repositoryTransaction;

	@Autowired
	ProductRepository repositoryProduct;

	@GetMapping(value="/api/transactions")
	public ResponseEntity<Response> getAllData(){
		List<Transaction> data = repositoryTransaction.findAll();
		if(data.size()==0) { //Handle Error
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response<Transaction>(500,"Data Not Found",data));
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(new Response<Transaction>(200, "Data of Produts Found", data));
		}
	}

	
	@PostMapping(value="/api/transaction", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> createTransaction(@RequestBody Transaction payload){
		int id = payload.getProducts().getId();
		
		List<Transaction> data = new ArrayList<Transaction>();
		if(validateTransaction(payload)) { //validating Transaction
			
			Product product = repositoryProduct.getProductById(id);
			int newStock =  product.getStock() - payload.getQty();
			repositoryProduct.updateStockByID(newStock, id);
			product.setStock(newStock);
			String timeStamps = generateDate(LocalDateTime.now());
			int totalPrice = getTotalPrice(payload.getQty(), product.getPrice());


			Transaction validatedTransaction = new Transaction(product, payload.getQty(), totalPrice, timeStamps);

			data.add(validatedTransaction);
			repositoryTransaction.saveAll(data);
			
			if(data.size()!=0){
				return ResponseEntity.status(HttpStatus.CREATED).body(new Response<Transaction>(201, "DATA CREATED", data));
			}
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response<Transaction>(500, "CREATE TRANSACTION FAILED : Stock Unavailable", data));
	}


	@PostMapping(value="/api/payment/search")
	public ResponseEntity<Response> paymentSearch(@RequestBody Transaction payload){
		List<Transaction> data = repositoryTransaction.getTransactionByDate(payload.getCreated_at());
		if(data.size()>0) {
			return ResponseEntity.status(HttpStatus.OK).body(new Response<Transaction>(200, "DATA FOUND", data));
		}else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response<Transaction>(500, "DATA NOT FOUND", data));
		}

	}

	public String generateDate(LocalDate dateToFormat) {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("YYYY-MM-dd");
		String fDTime = dateToFormat.format(format);
		return fDTime;
	}

	public boolean validateTransaction(Transaction payload) {
		int id = payload.getProducts().getId();
		Product product = repositoryProduct.getProductById(id);
		
		if(product.getStock() > payload.getQty() && product.getStock() >= 0) { //validating Transaction
			
			
				return true;
			
		
		}
		return false;
	}


	public int getTotalPrice (int qty, int price) {
		return qty*price;
	}

	public String generateDate(LocalDateTime dateToFormat) {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss");
		String fDTime = dateToFormat.format(format);
		return fDTime;
	}
}
