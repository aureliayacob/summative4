package com.summative4.NexMart.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.summative4.NexMart.entity.Product;
import com.summative4.NexMart.services.ProductRepository;
import com.summative4.NexMart.services.Response;
@RestController
public class ProductController {
	@Autowired
	ProductRepository repo;


	@GetMapping(value="/api/products")
	public ResponseEntity<Response> getAllData(){
		List<Product> data = repo.findAll();
		if(data.size()==0) { //Handle Error
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response<Product>(500,"Data Not Found",data));
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(new Response<Product>(200, "Data of Produts Found", data));
		}
	}

	@PostMapping(value="/api/product", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> createProduct(@RequestBody List<Product> payload){
		/*(Bisa nambah lebih dr 1 sekaligus, masukan nme, category, stock, kembalian otomatis ada id dan crated _at) @RequestBody (/api/product) */
		List<Product> saveToDB = new ArrayList<Product>();
		Product validatedProduct;
		for(Product p : payload) {
			if(validatingStock(p.getStock())) { //validating stock
				String timeStamps = generateDate(LocalDateTime.now());
				validatedProduct = new Product(p.getProduct_name(), p.getCategory(),p.getStock(),p.getPrice(),timeStamps);
				saveToDB.add(validatedProduct);
			}else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response<Product>(500, "CREATE DATA FAILED : Stock Invalid, Input must be at least 1 or All Fields must be inputed", saveToDB));
			}
		}

		if(saveToDB.size()!=0) {
			//save to db
			repo.saveAll(saveToDB);
			return ResponseEntity.status(HttpStatus.CREATED).body(new Response<Product>(201, "DATA CREATED", saveToDB));
		}else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response<Product>(500, "CREATE DATA FAILED : No Data Inputed", saveToDB));
		}
	}

	@PutMapping(value = "/api/product/{id}")
	public ResponseEntity<Response> editProduct(@PathVariable int id, @RequestBody Product payload){ //masih 1 payload
		//			int update = repo.saveAndFlush(null);
		List<Product> data = new ArrayList<Product>();

		if(validatingStock(payload.getStock())){
			int update = repo.updateByID(payload.getProduct_name(),payload.getCategory(), payload.getStock(), payload.getPrice(), id);

			if(update>0) { //Handle Error
				data.add(repo.getProductById(id));
				return ResponseEntity.status(HttpStatus.OK).body(new Response<Product>(201, "EDIT SUCCESS", data));

			}else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response<Product>(500,"EDIT FAILED, ID Invalid",data));
				
			}
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response<Product>(500,"EDIT FAILED, Stock Invalid",data));
		}
	}


	@DeleteMapping(value="/api/product/{id}")
	public ResponseEntity<Response> delteProduct(@PathVariable int id){
		Product deleted = repo.getProductById(id);
		int delete = repo.deleteProductById(id);
		//repo.deleteById(Long.valueOf(id));
		List<Product> data = new ArrayList<Product>();
		if(delete>0) { //Handle Error
			data.add(deleted);
			return ResponseEntity.status(HttpStatus.OK).body(new Response<Product>(201, "DELETE SUCCESS", data));

		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response<Product>(500,"DELETE FAILED : ID not found",data));
	}

	@PostMapping(value="/api/products/search")
	public ResponseEntity<Response> searchProduct(@RequestParam Map<String, String> params){
		String byStock=params.get("stock");
		String byName = params.get("name");
		String byCategory=params.get("category");
		List<Product> data = new ArrayList<Product>();

		if(byStock!=null && byName==null&&byCategory==null){ //byStock only
			data = repo.getProductbyStock(Integer.parseInt(byStock));
		}else if(byStock==null && byName!=null&&byCategory==null) {//byName
			data = repo.getProductbyName(byName);
		}else if(byStock==null && byName==null&&byCategory!=null) {
			data = repo.getProductbyCategory(byCategory);
		}else if(byStock!=null && byName!=null&&byCategory==null) { //stock & name
			data = repo.getProductbyStockName(Integer.parseInt(byStock), byName);
		}else if(byStock!=null && byName==null&&byCategory!=null) { //stock & category
			data = repo.getProductbyStockCategory(Integer.parseInt(byStock), byCategory);
		}else if(byStock==null && byName!=null&&byCategory!=null) { //stock & category
			data = repo.getProductbyNameCategory(byName, byCategory);
		}else if(byStock!=null && byName!=null&&byCategory!=null) {
			data = repo.getProductbyAllKeys(Integer.parseInt(byStock), byName, byCategory);
		}

		if(data.size()>0) { //Handle Error
			return ResponseEntity.status(HttpStatus.OK).body(new Response<Product>(201, "DATA FOUND", data));
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response<Product>(500,"DATA NOT FOUND",data));
	}


	public String generateDate(LocalDateTime dateToFormat) {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss");
		String fDTime = dateToFormat.format(format);
		return fDTime;
	}

	public boolean validatingStock(int stockToValidate) {
		if(stockToValidate<=0) {
			return false;
		}else {
			return true;
		}

	}



}
