package com.summative4.NexMart.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import com.summative4.NexMart.entity.Transaction;
import com.summative4.NexMart.services.TransactionRepository;

class TransactionControllerTest {
	
	@Autowired
	TransactionRepository repositoryTransaction;

	
	@Test
	public void getAllDataTest_NormalCase(){
		List<Transaction> data = repositoryTransaction.findAll();
		assertTrue(data.size()>0);
		
	}
	
	

}
