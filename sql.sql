CREATE TABLE summative4db.Product (
id INT NOT NULL AUTO_INCREMENT,
product_name VARCHAR (45),
category VARCHAR (45),
stock INT NOT NULL,
price INT,
created_at DATETIME,
PRIMARY KEY (id));

CREATE TABLE summative4db.`Transaction`(
id INT NOT NULL AUTO_INCREMENT,
product_id INT,
qty INT NOT NULL,
total_price INT NOT NULL,
created_at DATETIME,
PRIMARY KEY (id),
FOREIGN KEY(product_id) REFERENCES Product(id));

INSERT INTO Products (product_name, category, stock, price, created_at)VALUE ("Goodtime chocochips", "Snacks", 200, 15000, NOW());
delete from product;
select * from transaction;
select * from product;

SELECT * FROM product WHERE product_name LIKE '%Aqua%';

UPDATE product  set product_name="Pristine", category="Drink", stock=150, price=5000 where id= 5; 

SELECT * FROM Transaction  WHERE created_at LIKE  '%2021-10-25%';